describe('Authentication', () => {
	beforeEach(() => {
		cy.visit('http://localhost:3000');
	});

	it('should not show errors on initial render', () => {
		cy.get('.username-error').should('not.exist');
		cy.get('.password-error').should('not.exist');
		cy.get('.alert-danger').should('not.exist');
	});

	it('typed and cleared inputs should show errors', () => {
		cy.findByPlaceholderText('username').type('username');
		cy.findByPlaceholderText('username').clear();
		cy.get('.username-error').should('be.visible');
		cy.get('.username-error').contains('Username cannot be empty');
		cy.get('.password-error').should('not.exist');
		cy.findByPlaceholderText('username').type('username');
		cy.get('.username-error').should('not.exist');
		cy.get('.password-error').should('not.exist');

		cy.findByPlaceholderText('password').type('password');
		cy.findByPlaceholderText('password').clear();
		cy.get('.password-error').should('be.visible');
		cy.get('.password-error').contains('Password cannot be empty');
		cy.get('.username-error').should('not.exist');
		cy.findByPlaceholderText('password').type('password');
		cy.get('.password-error').should('not.exist');
		cy.get('.username-error').should('not.exist');

		cy.findByPlaceholderText('username').clear();
		cy.findByPlaceholderText('password').clear();
		cy.get('.username-error').should('be.visible');
		cy.get('.password-error').should('be.visible');
		
		cy.get('.username-error').contains('Username cannot be empty');
		cy.get('.password-error').contains('Password cannot be empty');
	});

	it('invalid credentials should show errors', () => {
		cy.findByPlaceholderText('username').type('username');
		cy.findByPlaceholderText('password').type('password');

		cy.findByDisplayValue('Log in').click();
		cy.get('.alert-danger').should('be.visible');
		cy.get('.alert-danger').contains('Invalid username/password combination.');
	});

	it('valid credentials should show servers list', () => {
		cy.findByPlaceholderText('username').type('tesonet');
		cy.findByPlaceholderText('password').type('partyanimal');

		cy.findByDisplayValue('Log in').click();

		cy.get('.servers-table').should('be.visible');
	});

});