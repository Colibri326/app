describe('Authentication', () => {
	beforeEach(() => {
		cy.visit('http://localhost:3000');

		cy.findByPlaceholderText('username').type('tesonet');
		cy.findByPlaceholderText('password').type('partyanimal');

		cy.findByDisplayValue('Log in').click();

		cy.get('.servers-table').should('be.visible');
	});

	it('sort arrows should be gray and point down on initial list render', () => {
		cy.get('.sort-by-name > .arrow').should('have.class', 'down border-gray-400');
		cy.get('.sort-by-distance > .arrow').should('have.class', 'down border-gray-400');
	});

	it('arrow should by cyon color and point in the same diretion after first click, but in oposite after second', () => {
		cy.get('.sort-by-name').click();
		cy.get('.sort-by-name > .arrow').should('have.class', 'down border-cyan-400');
		cy.get('.sort-by-name').click();
		cy.get('.sort-by-name > .arrow').should('have.class', 'up border-cyan-400');
		cy.get('.sort-by-name').click();
		cy.get('.sort-by-name > .arrow').should('have.class', 'down border-cyan-400');

		cy.get('.sort-by-distance').click();
		cy.get('.sort-by-distance > .arrow').should('have.class', 'down border-cyan-400');
		cy.get('.sort-by-distance').click();
		cy.get('.sort-by-distance > .arrow').should('have.class', 'up border-cyan-400');
		cy.get('.sort-by-distance').click();
		cy.get('.sort-by-distance > .arrow').should('have.class', 'down border-cyan-400');
	});

});