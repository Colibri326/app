const Loader = () => {
	return (
		<div className="spinner">
			<div className="cube1 bg-cyan-700"></div>
			<div className="cube2 bg-cyan-700"></div>
		</div>
	)
}

export default Loader;
