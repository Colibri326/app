import { useState } from 'react';
import Alert from './Alert';
import CustomRequest from '../Helpers/Request';

const Login = ({logIn}) => {
	const [ username, setUsername ] = useState('');
	const [ password, setPassword ] = useState('');
	const [ mainErrorMessage, setMainErrorMessage ] = useState('');
	const [ errors, setErrors ] = useState({
		username: '',
		password: ''
	});

	const formErrors = {
		emptyValue: {
			username: 'Username cannot be empty',
			password: 'Password cannot be empty'
		}
	}

	const validateInput = (name, value, setState) => {
		setState(value);

		const validationErrors = errors;

		validationErrors[name] = value === '' ?  formErrors.emptyValue[name] : '';

		setErrors(validationErrors);
	}

	const inputsValid = () => {
		let isValid = true;
		const validationErrors = {...errors};
		
		if (username === '') {
			validationErrors.username = formErrors.emptyValue.username;
			isValid = false;
		}

		if (password === '') {
			validationErrors.password = formErrors.emptyValue.password;
			isValid = false;
		}

		if (!isValid) {
			setErrors(validationErrors);
		}

		return isValid;
	}

	const onSubmit = e => {
		e.preventDefault();

		if (!inputsValid()) {
			return;
		}

		const credentials = {
			username: username,
			password: password
		}

		fetchToken(credentials).then(response => {
			if ((response.status === 200) && response.data.token) {
				localStorage.setItem('token', response.data.token);
				logIn();
			} else if (response.status === 401) {
				setMainErrorMessage('Invalid username/password combination.');
			} else {
				setMainErrorMessage('Something went wrong.');
			}
		}).catch(e => console.error(e));
	}

	const fetchToken = credentials => CustomRequest('https://playground.tesonet.lt/v1/tokens', credentials)
		.catch(() => setMainErrorMessage('Cannot connect to the server.'));

	return (
		<>
			{ mainErrorMessage && <Alert message={mainErrorMessage} type={'danger'} /> }
			
			<form className="login-form flex flex-col w-64 mx-auto my-32" onSubmit={onSubmit}>
				<input 
					className="p-2 m-1 bg-slate-900 text-white focus:outline-none focus:border-cyan-700 focus:ring-2 rounded"
					type="text" 
					placeholder="username"
					value={username}
					onChange={e => validateInput('username', e.target.value, setUsername)}
					/>
				{ errors.username && <span className="username-error text-red-400 font-medium text-center">{errors.username}</span> }
				<input 
					className="p-2 m-1 bg-slate-900 text-white focus:outline-none focus:border-cyan-700 focus:ring-2 rounded"
					type="password" 
					placeholder="password"
					value={password}
					onChange={e => validateInput('password', e.target.value, setPassword)}
					/>
				{ errors.password && <span className="password-error text-red-400 font-medium text-center">{errors.password}</span> }
				<input className="p-2 mx-1 mt-3 focus:outline-none focus:border-cyan-700 focus:ring-1 border-cyan-700 hover:bg-cyan-700 text-white rounded cursor-pointer border" type="submit" value="Log in"/>
			</form>
		</>

	)
}

export default Login;
