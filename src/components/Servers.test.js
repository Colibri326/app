import { render } from '@testing-library/react';
import Servers from './Servers';

const getServers = (sortData) => {
	const serversList = [
		{
			name: 'sample country', 
			distance: '12'
		}, 
		{
			name: 'sample another country', 
			distance: '100'
		}
	];

	return render(<Servers serverList={serversList} sortData={sortData} sort={() => {}} />);
}

test('shows servers in list', () => {
	const { container } = getServers({order: '', element: ''});
	const serversListWrapper = container.querySelector('.servers-list');
	const serversListItems = serversListWrapper.querySelectorAll('.servers-list-item');

	expect(serversListItems.length).toBe(2);

	expect(serversListWrapper).toHaveTextContent('sample country');
	expect(serversListWrapper).toHaveTextContent('sample another country');
});

describe('sorting arrows', () => {
	test('arrows should point downwards and be gray on initial render', () => {
		const { container } = getServers({order: '', element: ''});

		const nameSortArrow = container.querySelector('.sort-by-name > .arrow');
		expect(nameSortArrow).toHaveClass('down border-gray-400');
		expect(nameSortArrow).not.toHaveClass('border-cyan-400');
		expect(nameSortArrow).not.toHaveClass('up');

		const distanceSortArrow = container.querySelector('.sort-by-distance > .arrow');
		expect(distanceSortArrow).toHaveClass('down border-gray-400');
		expect(distanceSortArrow).not.toHaveClass('border-cyan-400');
		expect(distanceSortArrow).not.toHaveClass('up');
	});

	test('should highlight distance sort arrow, and point it downwards', () => {
		const { container } = getServers({order: 'asc', element: 'distance'});
	
		const distanceSortArrow = container.querySelector('.sort-by-distance > .arrow');
		expect(distanceSortArrow).toHaveClass('down border-cyan-400');

		const nameSortArrow = container.querySelector('.sort-by-name > .arrow');
		expect(nameSortArrow).not.toHaveClass('border-cyan-400');
		expect(nameSortArrow).toHaveClass('down');
	});

	test('should highlight name sort arrow, and point it upwards', () => {
		const { container } = getServers({order: 'desc', element: 'name'});
	
		const nameSortArrow = container.querySelector('.sort-by-name > .arrow');
		expect(nameSortArrow).toHaveClass('up border-cyan-400');
	});
});
