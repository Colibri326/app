const Servers = ({serverList, sort, sortData}) => {
	const getSortArrowClasses = (name) => {
		let classes = '';

		if (sortData.element === name) {
			if (sortData.order === 'desc') {
				classes += 'up';
			} else {
				classes += 'down';
			}

			classes += ' border-cyan-400'
		} else {
			classes += 'down border-gray-400'
		}

		return classes;
	}

	return (
		<div className=''>
			<table className='servers-table mx-auto text-left my-24 w-full text-gray-400 bg-slate-900'>
				<thead>
					<tr>
						<th className='sort-by-name servers-table-cel cursor-pointer select-none text-xl' onClick={() => sort('name')}>Name  <i className={`float-right arrow ${getSortArrowClasses('name')}`}></i></th>
						<th className='sort-by-distance servers-table-cel cursor-pointer select-none text-xl' onClick={() => sort('distance')}>Distance <i className={`float-right arrow ${getSortArrowClasses('distance')}`}></i></th>
					</tr>
				</thead>
				<tbody className="servers-list">
					{ serverList.map((server, index) => (
						<tr className="servers-list-item" key={index}>
							<th className='servers-table-cel'>{server.name}</th>
							<th className='servers-table-cel'>{server.distance}</th>
						</tr>
					))}
				</tbody>
			</table>
		</div>
	)
}

export default Servers;
