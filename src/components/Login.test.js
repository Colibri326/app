import { render, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Login from './Login';

test('on initial render errors should not be visible', () => {
	const { container } = render(<Login />);

	const usernameError = container.querySelector('.username-error');
	expect(usernameError).toBeNull();
	const passwordError = container.querySelector('.password-error');
	expect(passwordError).toBeNull();
	const mainError = container.querySelector('.alert-danger');
	expect(mainError).toBeNull();
});

test('if user typed and deleted input values, errors should be visible', () => {
	const { container } = render(<Login />);

	const usernameInput = screen.getByPlaceholderText('username');
	userEvent.type(usernameInput, 'username');
	expect(usernameInput).toHaveValue('username');
	userEvent.clear(usernameInput);
	expect(usernameInput).toHaveValue('');
	let usernameError = container.querySelector('.username-error');
	expect(usernameError).toHaveTextContent('Username cannot be empty');
	let passwordError = container.querySelector('.password-error');
	expect(passwordError).toBeNull();

	const passwordInput = screen.getByPlaceholderText('password');
	userEvent.type(passwordInput, 'password');
	expect(passwordInput).toHaveValue('password');
	userEvent.clear(passwordInput);
	expect(passwordInput).toHaveValue('');
	passwordError = container.querySelector('.password-error');
	expect(passwordError).toHaveTextContent('Password cannot be empty');
	usernameError = container.querySelector('.username-error');
	expect(usernameError).toHaveTextContent('Username cannot be empty');

	userEvent.type(usernameInput, 'username');
	expect(usernameInput).toHaveValue('username');
	usernameError = container.querySelector('.username-error');
	expect(usernameError).toBeNull();
	passwordError = container.querySelector('.password-error');
	expect(passwordError).toHaveTextContent('Password cannot be empty');
	
	userEvent.type(passwordInput, 'password');
	expect(passwordInput).toHaveValue('password');
	usernameError = container.querySelector('.username-error');
	expect(usernameError).toBeNull();
	passwordError = container.querySelector('.password-error');
	expect(passwordError).toBeNull();
});

test('invalid credentials should show error message', async () => {
	const { container } = render(<Login />);

	const usernameInput = screen.getByPlaceholderText('username');
	userEvent.type(usernameInput, 'username');
	const passwordInput = screen.getByPlaceholderText('password');
	userEvent.type(passwordInput, 'password');

	const loginButton = screen.getByDisplayValue('Log in');
	userEvent.click(loginButton);

	await waitFor(() => {
		const mainError = container.querySelector('.alert-danger');
		expect(mainError).toHaveTextContent('Invalid username/password combination.');
	});
});
