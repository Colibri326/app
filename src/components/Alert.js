const Alert = ({message, type}) => {
	return <div className={`alert-${type} w-2/4`}>{message}</div>
}

export default Alert;
