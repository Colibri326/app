const CustomRequest = (url = '', data = {}) => {
	if (!url) {
		return;
	}

	const request = {
		method: 'get',
		headers: {
			'Content-type': 'Application/json'
		}
	}

	let hasData = false;

	for (let name in data) {
		hasData = true;
	}
	
	if (hasData) {
		request.method = 'post';
		request.body = JSON.stringify(data)
	}

	const token = localStorage.getItem('token');

	if (token) {
		request.headers['Authorization'] = `Bearer ${token}`
	}

	return fetch(url, request).then(response => {
		const responseStatus = response.status;

		return response.json().then(data => {
			return {
				status: responseStatus,
				data: data
			}
		});
	});
}

export default CustomRequest;
