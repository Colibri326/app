import { render, waitFor } from '@testing-library/react';
import App from './App';

test('on initial render loader should be displayed ', () => {
	const { container } = render(<App />);

	const loader = container.querySelector('.spinner');
	expect(loader).toBeVisible();
});

test('after loading login screen should be visible', async () => {
	const { container } = render(<App />);

	await waitFor(() => {
		const usernameInput = container.querySelector('.login-form');
		expect(usernameInput).toBeVisible();
	});
});
