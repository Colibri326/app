import { useState, useEffect } from 'react';
import Login from './components/Login';
import Servers from './components/Servers';
import Loader from './components/Loader';
import Alert from './components/Alert';
import CustomRequest from './Helpers/Request';

function App() {
	const [ authorized, setAuthorization ] = useState(false);
	const [ serverList, setServerList ] = useState();
	const [ sortOrder, setSortOrder ] = useState('');
	const [ currentSortElemment, setCurrentSortElemment ] = useState('');
	const [ isLoading, setIsLoading ] = useState(true);
	const [ error, setError ] = useState('');

	useEffect(() => onLogIn(), []);

	const fetchServers = () => CustomRequest('https://playground.tesonet.lt/v1/servers')
		.catch(() => setError('Cannot connect to the server.'));

	const onLogIn = () => {
		setIsLoading(true);

		fetchServers().then(response => {
			if (response.status === 200) {
				setAuthorization(true);
				setServerList(response.data);
			} else if (response.status === 401) {
				setAuthorization(false);
			}	
		}).catch(e => console.error(e))
		.then(() => {
			setIsLoading(false);
		});
	}

	const onSort = name => {
		let currentSortOrder = sortOrder;
		if (name != currentSortElemment) {
			currentSortOrder = 'desc';
		}

		setCurrentSortElemment(name);

		const sorted = [...serverList].sort((a, b) => {
			if (currentSortOrder === 'asc') {
				return a[name] > b[name] ? 1 : -1;
			} else {
				return a[name] > b[name] ? -1 : 1;
			}
		});

		setSortOrder(currentSortOrder === 'asc' ? 'desc' : 'asc');
		setServerList(sorted);
	}

	return (
		<div className="container mx-auto px-2">
			{ isLoading 
				? <Loader />
				: error !== ''
					? <Alert message={error} type={'danger'} />
					: authorized
						? <Servers serverList={serverList} sort={onSort} sortData={{order: sortOrder, element: currentSortElemment}} />
						: <Login logIn={onLogIn} />
			}
		</div>
	);
}

export default App;
